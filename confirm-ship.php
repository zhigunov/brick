<?php
require_once( 'wp-load.php' );
if( is_user_logged_in() != true ){?>
    <h1>You are not logged in, please login</h1>
    <a href="/wp-login.php">Sign In</a>
<?php }else{
if(!isset($_POST)){
	die;
}

global $wpdb;
$user = wp_get_current_user();
$accessB = $wpdb->get_results("SELECT * FROM wp_brick_access WHERE user_id = ".$user->ID);
$accessS = $wpdb->get_results("SELECT * FROM wp_ship_access WHERE user_id = ".$user->ID);

	foreach ($_POST['to_brick'] as $order_id) {	
		$wpdb->query("INSERT INTO `wp_log_ship_to_brick`(`order_id`, `date`) VALUES (".$order_id.", '".date("d.m.Y")."')");

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bricklink.com/api/store/v2/orders/".$order_id."/status",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PUT",
		  CURLOPT_POSTFIELDS => "{\r\n    \"field\" : \"status\",\r\n    \"value\" : \"SHIPPED\" \r\n}",
		  CURLOPT_HTTPHEADER => array(
		    "authorization: ".generateAuth('PUT', 'https://api.bricklink.com/api/store/v2/orders/'.$order_id.'/status', array(), $accessB),
		    "cache-control: no-cache",
		    "content-type: application/json",
		    "postman-token: 5822ac13-4ec9-80f2-6fe0-023a9a15250b"
		  ),
		));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $username = $accessS[0]->login;
        $password = $accessS[0]->password;        
        $host_api = "https://ssapi.shipstation.com/shipments";
        $param = 'orderNumber='. $order_id;   
         
        $curl = curl_init($host_api);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);       
        // get запрос
        curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
        $shipped_arr = json_decode($result)->shipments;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.bricklink.com/api/store/v2/orders/".$order_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => "{\r\n    \"shipping\": {\r\n        \"tracking_no\": \"".$shipped_arr[0]->trackingNumber."\"\r\n    }\r\n}}",
          CURLOPT_HTTPHEADER => array(
            "authorization: ".generateAuth('PUT', 'https://api.bricklink.com/api/store/v2/orders/'.$order_id, array(), $accessB),
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 5822ac13-4ec9-80f2-6fe0-023a9a15250b"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
	}

?>
	<h1>Success!</h1>
	<a href="/">Back to dashboard</a>


<?php } ?>
<?php
	function generatePassword($length = 8){
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}

	function generateAuth($method, $url, $params, $accessB)
    {
    	$oauth = array(
            'oauth_consumer_key' => $accessB[0]->ConsumerKey,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => (string)time(),
            'oauth_nonce' => md5(mt_rand()),
            'oauth_version' => '1.0',
            'oauth_token' => $accessB[0]->TokenValue
        );

        $oauth = array_merge($oauth, $params);
        $baseStr = generateBaseString($method, $url, $oauth);

        $oauth['oauth_signature'] = generateSignature($baseStr, $accessB);
        ksort($oauth);

        $authHeader = 'OAuth ';
        foreach ($oauth as $key => $value) {
            $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        return substr($authHeader, 0, -2);
    }

    function generateBaseString($method, $url, $params)
    {
        $url = parse_url($url);
        if (isset($url['query'])) {
            parse_str($url['query'], $params2);
            $params = array_merge($params, $params2);
        }
        ksort($params);
        $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
        $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
        foreach ($params as $key => $value) {
            $baseStr .= rawurlencode(
                rawurlencode($key) . '=' . rawurlencode($value) . '&'
            );
        }
        return substr($baseStr, 0, -3);
    }

	function generateSignature($baseStr, $accessB)
    {
        
        $signingKey =  $accessB[0]->ConsumerSecret . '&' . $accessB[0]->TokenSecret;
        return base64_encode(
            hash_hmac(
                'sha1',
                $baseStr,
                $signingKey,
                true
            )
        );
    }

?>