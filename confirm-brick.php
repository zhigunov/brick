<?php
require_once( 'wp-load.php' );

nocache_headers();

if( is_user_logged_in() != true ){?>
	<h1>You are not logged in, please login</h1>
	<a href="/wp-login.php">Sign In</a>
<?php }else{

global $wpdb;
	$user = wp_get_current_user();
	$accessB = $wpdb->get_results("SELECT * FROM wp_brick_access WHERE user_id = ".$user->ID);
	$accessS = $wpdb->get_results("SELECT * FROM wp_ship_access WHERE user_id = ".$user->ID);

if(isset($_POST['confirm'])){
	// var_dump($_POST);
	// die;
	

	foreach ($_POST['order'] as $order_id => $order) {

		$url = 'https://api.bricklink.com/api/store/v2/orders/' . $order_id;
		if(function_exists('curl_init')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/' . $order_id, array(), $accessB)
			));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch);
			echo curl_error($ch);
			curl_close($ch);
			$order_obj = json_decode($output)->data;
		}
		// echo '<pre>';	
		// var_dump($order_obj);

		$url = 'https://api.bricklink.com/api/store/v2/orders/'.$order_id.'/items';
		if(function_exists('curl_init')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/'.$order_id.'/items', array(), $accessB)
			));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch);
			echo curl_error($ch);
			curl_close($ch);
			$items_arr = json_decode($output)->data;
		}
		// echo '<pre>';	
		// var_dump($items_arr);

		
		$wpdb->query("INSERT INTO `wp_log_brick_to_ship`(`order_id`, `date`) VALUES (".$order_id.", '".date("d.m.Y")."')");

		$request = array();
		$request['orderNumber'] = (string)$order_id;
		$request['orderDate'] = $order['data'];
		$request['orderStatus'] = 'awaiting_shipment';
		$request['billTo'] = array(	'name'		=> 'bricklink',
									'company' 	=> null,
									'street1' 	=> null,
									'street2' 	=> null,
									'street3' 	=> null,
									'city' 	=> null,
									'state' 	=> null,
									'postalCode' 	=> null,
									'country' 	=> null,
									'phone' 	=> null,
									'residential' 	=> null,
								);
		$request['shipTo'] = array(	'name'		=> $order_obj->shipping->address->name->full,
									'company' 	=> null,
									'street1' 	=> $order_obj->shipping->address->address1,
									'street2' 	=> $order_obj->shipping->address->address2,
									'street3' 	=> null,
									'city' 	=> $order_obj->shipping->address->city,
									'state' 	=> $order_obj->shipping->address->state,
									'postalCode' 	=> $order_obj->shipping->address->postal_code,
									'country' 	=> $order_obj->shipping->address->country_code,
									'phone' 	=> null,
									'residential' 	=> null,
								);
		$request['items'] = array();
		$items_price = 0;
		foreach ($items_arr as $item_arr) {
			foreach ($item_arr as $item) {
				if (in_array($item->inventory_id, $_POST['selectitem'][$order_id])) { 
					$request['items'][] = array('name' => $item->item->name,
												'sku' => $item->remarks,
												'weight' => array('value' => $item->weight, 'units' => 'grams'),
												'quantity' => $item->quantity,
												'unitPrice' => $item->unit_price_final,
												'productId' => $item->inventory_id
											);
					$items_price += $item->unit_price * $item->quantity;
				}
			}
		}
		$request['amountPaid'] = $items_price + $order_obj->cost->shipping;
		$request['shippingAmount'] = $order_obj->cost->shipping;
		$request['tagId'] = array(44285);

		$shipObj = json_encode($request);
		// $shipObj = str_replace('"', '\"', $shipObj);
		$shipObj = str_replace(':', ': ', $shipObj);
		// $shipObj = '['.$shipObj.']';

		// var_dump($shipObj);
		echo '<br><br>';

		$username = $accessS[0]->login;
		$password = $accessS[0]->password;   
		$host_api = "https://ssapi.shipstation.com/orders/createorder";
		$param = '';   
		 
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://ssapi.shipstation.com/orders/createorder");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $shipObj);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		  "Authorization: Basic ZjAyMzY0MTA3ZDk2NDgyYWJlZjE5MzBkZjViMDQzYzA6ZTMyYmQ4NWQ1ZTlmNDcwOWJmOTk1NTc0MzBmNWNmNTI="
		));

		$response = curl_exec($ch);
		curl_close($ch);

		

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://ssapi.shipstation.com/orders/addtag");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		curl_setopt($ch, CURLOPT_POST, TRUE);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "{
		  \"orderId\": ".json_decode($response)->orderId.",
		  \"tagId\": 44285
		}");

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		  "Authorization: Basic ZjAyMzY0MTA3ZDk2NDgyYWJlZjE5MzBkZjViMDQzYzA6ZTMyYmQ4NWQ1ZTlmNDcwOWJmOTk1NTc0MzBmNWNmNTI="
		));

		$response = curl_exec($ch);
		curl_close($ch);

		// var_dump($response);


		// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
		// curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);   
		// curl_setopt($curl, CURLOPT_HEADER, FALSE);
		// curl_setopt($curl, CURLOPT_POST, TRUE);    
		// curl_setopt($curl, CURLOPT_POSTFIELDS, $shipObj);
		// curl_setopt($curl, CURLOPT_URL, "$host_api");       
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		//   "Content-Type: application/json"
		// ));
		
		// $result = curl_exec($curl);
		// var_dump($result);
	}
	?>
	<h1>Success!</h1>
	<a href="/">Back to dashboard</a>

<?php
}else{
?>
	<style>
		.overlay {display:none; position:fixed; z-index:999; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000; }
		.popup {display:none; position:fixed; border:3px solid #999; background:rgb(114, 114, 114);; width:900px; height:700px; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index:1000;  padding:30px;}
		.popup-min{display: none; position: fixed; background: #f4f7f6; width: 500px; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 1000; padding: 30px;}
		.popup-banner{display: none; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); width: 90%; z-index: 1000; height: 0; padding-top: 121.857%;}
		.close-banner-visible {position: absolute; right: 5px; top: 5px; height: 40px; width: 40px; color: #000; text-align: center; margin-top: 0px; border: 1px solid; border-radius: 35px; font-size: 26px;}
		.close {display:block; width:24px; text-align:center; cursor:pointer;  height:24px; line-height:24px; background:#fff; color:red; position:absolute; top:10px; right:10px; text-decoration:none; font-size:20px; }

		.open_popup {text-decoration:underline; color:#00aeef; cursor:pointer; }
		.open_popup:hover {text-decoration:none;  }
	</style>
	<form method="POST">
		<input type="submit" value="Confirm">
		<input type="hidden" name="confirm" value="true">
		<table class="table table-checks" border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
			<tr>
				<td>Order #</td>
				<td>Status</td>
				<td>Total Items</td>
				<td>Unique Items (Lots)</td>
				<td>Shipping Method</td>
				<td>customer name</td>
				<td>customer address</td>
				<td>customer e-mail</td>
				<td>value</td>
				<td>shipping total</td>
				<td style="width: 100px;">order date</td>
				<td>Edit</td>
			</tr>
			<?php
			foreach ($_POST["to_ship"] as $order_id) { ?>
				<tr>
					<td><?php echo $order_id; ?></td>
					<td><?php echo $_POST[$order_id]['status']; ?></td>
					<td><?php echo $_POST[$order_id]['total_count']; ?></td>
					<td><?php echo $_POST[$order_id]['unique_count']; ?></td>
					<td><?php echo $_POST[$order_id]['method']; ?></td>
					<td><?php echo $_POST[$order_id]['buyer_name']; ?></td>
					<td><?php echo $_POST[$order_id]['address']; ?></td>
					<td><?php echo $_POST[$order_id]['buyer_email']; ?></td>
					<td><?php echo $_POST[$order_id]['grand_total']; ?></td>
					<td><?php echo $_POST[$order_id]['shipping']; ?></td>
					<td><?php echo explode('T', $_POST[$order_id]['data'])[0]; ?><br><?php echo explode('.',explode('T', $_POST[$order_id]['data'])[1])[0]; ?></td>
					<td class="open_popup" rel="<?php echo $order_id; ?>">Edit</td>

					<div id="overlay" class="overlay"></div>
			        <div class="popup-min" id="<?php echo $order_id; ?>">
			        	<?php 
	           			$url = 'https://api.bricklink.com/api/store/v2/orders/'.$order_id.'/items';
						if(function_exists('curl_init')) {
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL,$url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array(
								'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/'.$order_id.'/items', array(), $accessB)
							));
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							$output = curl_exec($ch);
							echo curl_error($ch);
							curl_close($ch);
							$items_arr = json_decode($output)->data;
						}
						// var_dump($output);
						?>
						<div>
			           			<!-- <tr>
				           			<td></td>
				           			<td>Name</td>
				           			<td>SKU</td>
				           		</tr> -->
				           		<div style="width: 5%; display: inline-block;">&nbsp;</div>
				           		<div style="width: 20%; display: inline-block;">SKU</div>
				           		<div style="width: 70%; display: inline-block;">Name</div>
				           		<?php
				           			foreach ($items_arr as $items_ar) { 
				           				foreach ($items_ar as $item) { 
				           					// var_dump($item);
					           				?>
							           		<div style="width: 5%; display: inline-block;">
							           			<span>
							           				<input type="checkbox" name="selectitem[<?php echo $order_id; ?>][]" value="<?php echo $item->inventory_id; ?>" checked="checked">
							           			</span>
							           		</div>
							           		<div style="width: 20%; display: inline-block;">
							           			<span><?php echo $item->remarks; ?></span>
							           		</div>
							           		<div style="width: 70%; display: inline-block;">
							           			<span><?php echo $item->item->name; ?></span>
							           		</div>
							           		<br>
		           				<?php 	
		           						}
				           			}
				           		?>
			           	</div>
			        </div>

					<input type="hidden" name="order[<?php echo $order_id; ?>][status]" value="<?php echo $_POST[$order_id]['status']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][total_count]" value="<?php echo $_POST[$order_id]['total_count']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][unique_count]" value="<?php echo $_POST[$order_id]['unique_count']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][method]" value="<?php echo $_POST[$order_id]['method']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][buyer_name]" value="<?php echo $_POST[$order_id]['buyer_name']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][address]" value="<?php echo $_POST[$order_id]['address']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][buyer_email]" value="<?php echo $_POST[$order_id]['buyer_email']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][grand_total]" value="<?php echo $_POST[$order_id]['grand_total']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][shipping]" value="<?php echo $_POST[$order_id]['shipping']; ?>">
					<input type="hidden" name="order[<?php echo $order_id; ?>][data]" value="<?php echo $_POST[$order_id]['data']; ?>">
				</tr>
			<?php
			} ?>
		</table>
	</form>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
		jQuery('.open_popup').click(function() {
            jQuery('.overlay, .popup, .popup-min, .popup-banner').hide();
            var popup_id = jQuery('#' + jQuery(this).attr("rel"));
            jQuery(popup_id).show();
            jQuery('.overlay').show();
        })
        jQuery('.overlay').click(function() {
            jQuery('.overlay, .popup, .popup-min, .popup-banner').hide();
        })
        jQuery('.close-banner').click(function() {
            jQuery('.overlay, .popup, .popup-min, .popup-banner').hide();
        })

        jQuery('.close-banner-visible').click(function() {
            jQuery('.overlay, .popup, .popup-min, .popup-banner').hide();
        })
	</script>
<?php
}
}
?>


<?php
	function generatePassword($length = 8){
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}

	function generateAuth($method, $url, $params, $accessB)
    {
    	$oauth = array(
            'oauth_consumer_key' => $accessB[0]->ConsumerKey,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => (string)time(),
            'oauth_nonce' => md5(mt_rand()),
            'oauth_version' => '1.0',
            'oauth_token' => $accessB[0]->TokenValue
        );

        $oauth = array_merge($oauth, $params);
        $baseStr = generateBaseString($method, $url, $oauth);

        $oauth['oauth_signature'] = generateSignature($baseStr, $accessB);
        ksort($oauth);

        $authHeader = 'OAuth ';
        foreach ($oauth as $key => $value) {
            $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        return substr($authHeader, 0, -2);
    }

    function generateBaseString($method, $url, $params)
    {
        $url = parse_url($url);
        if (isset($url['query'])) {
            parse_str($url['query'], $params2);
            $params = array_merge($params, $params2);
        }
        ksort($params);
        $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
        $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
        foreach ($params as $key => $value) {
            $baseStr .= rawurlencode(
                rawurlencode($key) . '=' . rawurlencode($value) . '&'
            );
        }
        return substr($baseStr, 0, -3);
    }

	function generateSignature($baseStr, $accessB)
    {
        
        $signingKey =  $accessB[0]->ConsumerSecret . '&' . $accessB[0]->TokenSecret;
        return base64_encode(
            hash_hmac(
                'sha1',
                $baseStr,
                $signingKey,
                true
            )
        );
    }

?>