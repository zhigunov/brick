<?php
require_once( '/home/mplaceconnect/marketplaceconnect.lefttwin.org/wp-load.php' );
global $wpdb;

$users = get_users();

foreach ($users as $user) {
	$accessB = $wpdb->get_results("SELECT * FROM wp_brick_access WHERE user_id = ".$user->data->ID);
	$accessE = $wpdb->get_results("SELECT * FROM wp_ecom_access WHERE user_id = ".$user->data->ID);

	if(!isset($accessE[0]->ecd_subscription_key)){
		continue;
	}

	$Ocp_Apim_Subscription_Key = "Ocp-Apim-Subscription-Key: " . $accessE[0]->Ocp_Apim_Subscription_Key;
	$ecd_subscription_key = "ecd-subscription-key: " . $accessE[0]->ecd_subscription_key;

	$url = 'https://api.bricklink.com/api/store/v2/orders?status=PAID';
	$arr = $wpdb->get_results("SELECT order_id FROM wp_log_brick_to_ecom WHERE 1");
	$brick_to_ecom = array();
	foreach ($arr as $value) {
		$brick_to_ecom[] = $value->order_id;
	}

	$updateOnEcomdash = array();

	if(function_exists('curl_init')) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders?status=PAID', array('status'=>"PAID"), $accessB)
		));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch);
		echo curl_error($ch);
		curl_close($ch);
	}
	$orders_arr = array_slice(array_reverse(json_decode($output)->data), 0, 20);
	if($orders_arr){
		foreach ($orders_arr as $orders) {
			if (!in_array($orders->order_id, $brick_to_ecom)) { 
				$wpdb->query("INSERT INTO `wp_log_brick_to_ecom`(`order_id`, `date`) VALUES (".$orders->order_id.", '".date("d.m.Y")."')");
			    $url = 'https://api.bricklink.com/api/store/v2/orders/'.$orders->order_id.'/items';
				if(function_exists('curl_init')) {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/'.$orders->order_id.'/items', array(), $accessB)
					));
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch);
					echo curl_error($ch);
					curl_close($ch);
					$items_arr = json_decode($output)->data;

					$wpdb->query("INSERT INTO `wp_log_brick_to_ship`(`order_id`, `date`) VALUES (".$order_id.", '".date("d.m.Y")."')");

					$request = array();
					$request['orderNumber'] = (string)$order_id;
					$request['orderDate'] = $order['data'];
					$request['orderStatus'] = 'awaiting_shipment';
					$request['billTo'] = array(	'name'		=> 'bricklink',
												'company' 	=> null,
												'street1' 	=> null,
												'street2' 	=> null,
												'street3' 	=> null,
												'city' 	=> null,
												'state' 	=> null,
												'postalCode' 	=> null,
												'country' 	=> null,
												'phone' 	=> null,
												'residential' 	=> null,
											);
					$request['shipTo'] = array(	'name'		=> $order_obj->shipping->address->name->full,
												'company' 	=> null,
												'street1' 	=> $order_obj->shipping->address->address1,
												'street2' 	=> $order_obj->shipping->address->address2,
												'street3' 	=> null,
												'city' 	=> $order_obj->shipping->address->city,
												'state' 	=> $order_obj->shipping->address->state,
												'postalCode' 	=> $order_obj->shipping->address->postal_code,
												'country' 	=> $order_obj->shipping->address->country_code,
												'phone' 	=> null,
												'residential' 	=> null,
											);
					$request['items'] = array();
					$items_price = 0;

					foreach ($items_arr as $batch) {
						foreach ($batch as $item) {
							$request['items'][] = array('name' => $item->item->name,
														'sku' => $item->remarks,
														'weight' => array('value' => $item->weight, 'units' => 'grams'),
														'quantity' => $item->quantity,
														'unitPrice' => $item->unit_price_final,
														'productId' => $item->inventory_id
													);
							$items_price += $item->unit_price * $item->quantity;

							//Получаем количество товара на складе
							$curl = curl_init();
							curl_setopt_array($curl, array(
							  CURLOPT_URL => "https://ecomdash.azure-api.net/api/product?sku=" . urlencode($item->remarks),
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_CUSTOMREQUEST => "GET",
							  CURLOPT_HTTPHEADER => array(
							    "Cache-Control: no-cache",
							    $Ocp_Apim_Subscription_Key,
							    "Postman-Token: 0066b75a-9fc2-96dd-761c-a52004ff5a2e",
							    $ecd_subscription_key
							  ),
							));
							$response_json = curl_exec($curl);
							$err = curl_error($curl);
							curl_close($curl);
							$response = json_decode($response_json);
							if($response->Message){
								// Если нет СКУ на ЭКОМДАШЕ
								echo $response->Message . ' SKU = ' . $item->remarks . '<br>';
							}else{
								$quantityE = $response->QuantityOnHand - $item->quantity;
								if($quantityE < 0){
									$quantityE = 0;
								}
								$wpdb->query("UPDATE `wp_brick_inventory` SET `quantity`='".$item->quantity."' WHERE sku = '".$item->remarks."'");
								$updateOnEcomdash[] = array('Sku' => $item->remarks, 'Quantity' => $quantityE, 'WarehouseId' => 23423);
								
							}
						}
						$request['amountPaid'] = $items_price + $order_obj->cost->shipping;
						$request['shippingAmount'] = $order_obj->cost->shipping;
						$request['tagId'] = array(44285);

						$shipObj = json_encode($request);
						// $shipObj = str_replace('"', '\"', $shipObj);
						$shipObj = str_replace(':', ': ', $shipObj);
						// $shipObj = '['.$shipObj.']';

						// var_dump($shipObj);
						echo '<br><br>';

						$username = $accessS[0]->login;
						$password = $accessS[0]->password;   
						$host_api = "https://ssapi.shipstation.com/orders/createorder";
						$param = '';   
						 
						
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://ssapi.shipstation.com/orders/createorder");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
						curl_setopt($ch, CURLOPT_HEADER, FALSE);
						curl_setopt($ch, CURLOPT_POST, TRUE);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $shipObj);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						  "Content-Type: application/json",
						  "Authorization: Basic ZjAyMzY0MTA3ZDk2NDgyYWJlZjE5MzBkZjViMDQzYzA6ZTMyYmQ4NWQ1ZTlmNDcwOWJmOTk1NTc0MzBmNWNmNTI="
						));

						$response = curl_exec($ch);
						curl_close($ch);
						echo "<pre>Заказы--------------------------";
						var_dump($response);
		

						$ch = curl_init();

						curl_setopt($ch, CURLOPT_URL, "https://ssapi.shipstation.com/orders/addtag");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
						curl_setopt($ch, CURLOPT_HEADER, FALSE);

						curl_setopt($ch, CURLOPT_POST, TRUE);

						curl_setopt($ch, CURLOPT_POSTFIELDS, "{
						  \"orderId\": ".json_decode($response)->orderId.",
						  \"tagId\": 44285
						}");

						curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						  "Content-Type: application/json",
						  "Authorization: Basic ZjAyMzY0MTA3ZDk2NDgyYWJlZjE5MzBkZjViMDQzYzA6ZTMyYmQ4NWQ1ZTlmNDcwOWJmOTk1NTc0MzBmNWNmNTI="
						));

						$response = curl_exec($ch);
						curl_close($ch);
					}
				}
			} 
		}
	}

	// обновление остатков по ску на ЭКОМДАШЕ
	if($updateOnEcomdash == "qwe"){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://ecomdash.azure-api.net/api/inventory/updateQuantityOnHand",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($updateOnEcomdash),
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    $Ocp_Apim_Subscription_Key,
		    "Postman-Token: 0066b75a-9fc2-96dd-761c-a52004ff5a2e",
		    $ecd_subscription_key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}

////////////////////// получение остатков с экомдаша



/////////////////////запись инвентория в базу

	$items_arr = $wpdb->get_results("SELECT * FROM wp_brick_inventory WHERE 1");

    // ДЛЯ ТЕСТИРОВАНИЯ
    // $test_items_arr = array();
    // // print_r($items_arr);
    // foreach ($items_arr as $item) {
    // 	if($item->remarks == '* Dr Strange 76108'){
    // 		// echo '<pre>';
    // 		// var_dump($item);
    // 		$test_items_arr[] = $item;
    // 	}
    // // 	var_dump($item->inventory_id);
    // // 	var_dump($item->remarks);
    // // 	var_dump($item->quantity);
    // }
    // $items_arr = $test_items_arr;
    /////////////////////////////

	$pageNumber = 1;
	$TotalNumberOfPages=10000;
	while ($pageNumber <= $TotalNumberOfPages) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://ecomdash.azure-api.net/api/Inventory?resultsPerPage=200&pageNumber=".$pageNumber,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n<statusreq>\r\n  <auth extra=\"90\" login=\"GOODSTORIA\" pass=\"3638168\"></auth>\r\n  <changes>ONLY_LAST</changes>\r\n  <quickstatus>NO</quickstatus>\r\n</statusreq>",
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    $Ocp_Apim_Subscription_Key,
		    "Postman-Token: 0066b75a-9fc2-96dd-761c-a52004ff5a2e",
		    $ecd_subscription_key
		  ),
		));
		$response = curl_exec($curl);
		$obj = json_decode($response);
		$TotalNumberOfPages = $obj->pagination->totalNumberOfPages;
		if(!$TotalNumberOfPages){
			$TotalNumberOfPages = $obj->pagination->TotalNumberOfPages;
		}
		$pageNumber++;
		echo '<pre>';
		var_dump($TotalNumberOfPages);
		foreach ($obj->data as $product) {
			// var_dump($product->Sku); 
			foreach ($items_arr as $item) {			

		    	if(strcasecmp($product->Sku, $item->sku) == 0 && (int)$product->QuantityOnHand != (int)$item->quantity){
		    	

		    		echo '<br>-----------------------<br>';
		    		var_dump($item->sku);
		    		var_dump($item->quantity);
		    		var_dump($product->QuantityOnHand);
		    		$is_stock_room = "false";
		    		if((int)$product->QuantityOnHand == 0 || (int)$product->QuantityOnHand < 0){
		    			$is_stock_room = "true";
		    			$product->QuantityOnHand = 0;
		    		}

		    		//обновляем количество на брике
		    		$quantity = $product->QuantityOnHand - $item->quantity;
		    		
		    		var_dump((string)$quantity);
		    		$wpdb->query("UPDATE `wp_brick_inventory` SET `quantity`='".$product->QuantityOnHand."' WHERE sku = '".$item->sku."'");
		    		$curl = curl_init();

			        curl_setopt_array($curl, array(
			          CURLOPT_URL => "https://api.bricklink.com/api/store/v2/inventories/".$item->inventory_id,
			          CURLOPT_RETURNTRANSFER => true,
			          CURLOPT_ENCODING => "",
			          CURLOPT_MAXREDIRS => 10,
			          CURLOPT_TIMEOUT => 30,
			          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			          CURLOPT_CUSTOMREQUEST => "PUT",
			          CURLOPT_POSTFIELDS => "{\r\n    \"quantity\" : ".$quantity.",\r\n    \"is_stock_room\":" .$is_stock_room. "\r\n}",
			          CURLOPT_HTTPHEADER => array(
			            "authorization: ".generateAuth('PUT', 'https://api.bricklink.com/api/store/v2/inventories/'.$item->inventory_id, array(), $accessB),
			            "cache-control: no-cache",
			            "content-type: application/json",
			          ),
			        ));

			        $response = curl_exec($curl);
			        $err = curl_error($curl);
			        curl_close($curl);
			        var_dump($response);
		    	}
		    }
		}
	}

}
	
$f = fopen('/marketplaceconnect.lefttwin.org/test.txt', 'w');
fwrite($f, $response);
fclose($f);


	?>

<?php
	function generatePassword($length = 8){
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}

	function generateAuth($method, $url, $params, $accessB)
    {
    	$oauth = array(
            'oauth_consumer_key' => $accessB[0]->ConsumerKey,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => (string)time(),
            'oauth_nonce' => md5(mt_rand()),
            'oauth_version' => '1.0',
            'oauth_token' => $accessB[0]->TokenValue
        );

        $oauth = array_merge($oauth, $params);
        $baseStr = generateBaseString($method, $url, $oauth);

        $oauth['oauth_signature'] = generateSignature($baseStr, $accessB);
        ksort($oauth);

        $authHeader = 'OAuth ';
        foreach ($oauth as $key => $value) {
            $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        return substr($authHeader, 0, -2);
    }

    function generateBaseString($method, $url, $params)
    {
        $url = parse_url($url);
        if (isset($url['query'])) {
            parse_str($url['query'], $params2);
            $params = array_merge($params, $params2);
        }
        ksort($params);
        $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
        $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
        foreach ($params as $key => $value) {
            $baseStr .= rawurlencode(
                rawurlencode($key) . '=' . rawurlencode($value) . '&'
            );
        }
        return substr($baseStr, 0, -3);
    }

	function generateSignature($baseStr, $accessB)
    {
        
        $signingKey =  $accessB[0]->ConsumerSecret . '&' . $accessB[0]->TokenSecret;
        return base64_encode(
            hash_hmac(
                'sha1',
                $baseStr,
                $signingKey,
                true
            )
        );
    }

?>