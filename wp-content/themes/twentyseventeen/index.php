<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
if( is_user_logged_in() != true ){?>
	<h1>You are not logged in, please login</h1>
	<a href="/wp-login.php">Sign In</a>
<?php die; 
}

global $wpdb;
$user = wp_get_current_user();
$accessB = $wpdb->get_results("SELECT * FROM wp_brick_access WHERE user_id = ".$user->ID);
$accessS = $wpdb->get_results("SELECT * FROM wp_ship_access WHERE user_id = ".$user->ID);
?>
<script type="text/javascript">
	jQuery(function(){
		jQuery("#wr-tabs").on("click", ".tab", function(){
			var tabs = jQuery("#wr-tabs .tab"),
			    cont = jQuery("#wr-tabs .tab-cont");
			tabs.removeClass("active");
			cont.removeClass("active");
			jQuery(this).addClass("active");
			cont.eq(jQuery(this).index()).addClass("active");

			return false;
		});
	});
	jQuery(document).ready(function(){
	    var table = jQuery('table.table-checks');
	    table
	    .on('change', '#all', function(){
	    	jQuery('> tbody input:checkbox', table).prop('checked', jQuery(this).is(':checked')).trigger('change');   
	    });
	});
</script>
<!-- tabs  -->
<div id="wr-tabs">
	<div class="tabs">
		<div class="tab active">BrickLink</div>
		<div class="tab">ShipStation</div>
	</div>
	<div class="content">
		<div class="tab-cont active">
<!-- bricklink tab -->
<?php
	$url = 'https://api.bricklink.com/api/store/v2/orders?status=PAID';
	$arr = $wpdb->get_results("SELECT order_id FROM wp_log_brick_to_ship WHERE 1");
	$brick_to_ship = array();
	foreach ($arr as $value) {
		$brick_to_ship[] = $value->order_id;
	}

	$arr1 = $wpdb->get_results("SELECT order_id FROM wp_log_ship_to_brick WHERE 1");
	$ship_to_brick = array();
	foreach ($arr1 as $value) {
		$ship_to_brick[] = $value->order_id;
	}
?> 
	<form method="POST" action="confirm-brick.php">
		<input type="submit" value="Submit to ShipStation">
	<table class="table table-checks" border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
		<tr>
			<td style="width: 40px; text-align: center; padding: 0; position: relative;">
				<input style="margin: 0;" type="checkbox" id="all" />
			</td>
			<td>Order #</td>
			<td>Status</td>
			<td>Total Items</td>
			<td>Unique Items (Lots)</td>
			<td>Shipping Method</td>
			<td>customer name</td>
			<td>customer address</td>
			<td>customer e-mail</td>
			<td>value</td>
			<td>shipping total</td>
			<td style="width: 100px;">order date</td>
		</tr>
	<?php
	if(function_exists('curl_init')) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders?status=PAID', array('status'=>"PAID"), $accessB)
		));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch);
		echo curl_error($ch);
		curl_close($ch);
	}
	$orders_arr = array_slice(array_reverse(json_decode($output)->data), 0, 20);
	if($orders_arr){
		foreach ($orders_arr as $orders) {
			$url = 'https://api.bricklink.com/api/store/v2/orders/' . $orders->order_id;
			if(function_exists('curl_init')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/' . $orders->order_id, array(), $accessB)
				));
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$output = curl_exec($ch);
				echo curl_error($ch);
				curl_close($ch);
				$order_obj = json_decode($output)->data;
			}
			if (in_array($orders->order_id, $brick_to_ship)) { ?>
			    <tr style="background-color: green">
					<td style="width: 40px; text-align: center; padding: 0;"></td>
			<?php }else{ ?>
				<tr>
					<td style="width: 40px; text-align: center; padding: 0;"><span><input style="margin: 0;" type="checkbox" name="to_ship[]" value="<?php echo $orders->order_id; ?>" /></span></td>
			<?php } ?>		
				<td><?php echo $orders->order_id; ?></td>
				<td><?php echo $orders->status; ?></td>
				<td><?php echo $orders->total_count; ?></td>
				<td><?php echo $orders->unique_count; ?></td>
				<td><?php echo $order_obj->shipping->method; ?></td>
				<td><?php echo $order_obj->buyer_name; ?></td>
				<td><?php echo $order_obj->shipping->address->name->full . ' ' . $order_obj->shipping->address->full; ?></td>
				<td><?php echo $order_obj->buyer_email; ?></td>
				<td><?php echo $order_obj->cost->grand_total; ?></td>
				<td><?php echo $order_obj->cost->shipping; ?></td>
				<td><?php echo explode('T', $order_obj->date_ordered)[0]; ?><br><?php echo explode('.',explode('T', $order_obj->date_ordered)[1])[0]; ?></td>
				<input type="hidden" name="<?php echo $orders->order_id; ?>[status]" value="<?php echo $orders->status; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[total_count]" value="<?php echo $orders->total_count; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[unique_count]" value="<?php echo $orders->unique_count; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[method]" value="<?php echo $order_obj->shipping->method; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[buyer_name]" value="<?php echo $order_obj->buyer_name; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[address]" value="<?php echo $order_obj->shipping->address->name->full . ' ' . $order_obj->shipping->address->full; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[buyer_email]" value="<?php echo $order_obj->buyer_email; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[grand_total]" value="<?php echo $order_obj->cost->grand_total; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[shipping]" value="<?php echo $order_obj->cost->shipping; ?>">
				<input type="hidden" name="<?php echo $orders->order_id; ?>[data]" value="<?php echo $order_obj->date_ordered; ?>">
			</tr>
			<?php
		}
	}
	?>
	</table>
</form>
<!-- shipstation tab -->
		</div>
		<div class="tab-cont">
			<form method="POST" action="confirm-ship.php">
				<input type="submit" value="Submit to Bricklink">
			<table class="table table-checks" border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
	<tr>
		<td style="width: 40px; text-align: center; padding: 0; position: relative;">
			<input style="margin: 0;" type="checkbox" id="all" />
		</td>
		<td>Order #</td>
		<td>Tracking number</td>
		<td>Order status</td>
	</tr>
<?php
	$username = $accessS[0]->login;
	$password = $accessS[0]->password;     
	$host_api = "https://ssapi.shipstation.com/orders/listbytag";
	$param = 'orderStatus=awaiting_shipment&tagId=44285';   
	$curl = curl_init($host_api);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
	curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	$orders_arr = json_decode($result)->orders;
	foreach ($orders_arr as $orders) {
		?>
		<tr style="background-color: grey;">
			<td></td>
			<td data-id="<?php echo $orders->orderId; ?>"><?php echo $orders->orderNumber; ?></td>
			<td></td>
			<td><?php echo $orders->orderStatus; ?></td>
		</tr>
		<?php
	}
	$param = 'orderStatus=shipped&tagId=44285&pageSize=50';   

	$curl = curl_init($host_api);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
	curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	$orders_arr = json_decode($result)->orders;
	if($orders_arr){
		foreach ($orders_arr as $orders) {
			$host_api = "https://ssapi.shipstation.com/shipments";
			$param = 'orderNumber='. $orders->orderNumber;   
			 
			$curl = curl_init($host_api);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
			curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
			curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($curl);
			$shipped_arr = json_decode($result)->shipments;
			if (in_array($orders->orderNumber, $ship_to_brick)) { 
				continue;
			 }else{ ?>
				<tr style="background-color: yellow;">
					<td style="width: 40px; text-align: center; padding: 0;"><span><input style="margin: 0;" type="checkbox" name="to_brick[]" value="<?php echo $orders->orderNumber; ?>" /></span></td>
			<?php } ?>	
				<td><?php echo $orders->orderNumber; ?></td>
				<td><?php echo $shipped_arr[0]->trackingNumber; ?></td>
				<td><?php echo $orders->orderStatus; ?></td>
			</tr>
			<?php
		}
	}
?>
</table>
</form>
		</div>
	</div>
</div>

<?php
	function generatePassword($length = 8){
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}

	function generateAuth($method, $url, $params, $accessB)
    {
    	$oauth = array(
            'oauth_consumer_key' => $accessB[0]->ConsumerKey,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => (string)time(),
            'oauth_nonce' => md5(mt_rand()),
            'oauth_version' => '1.0',
            'oauth_token' => $accessB[0]->TokenValue
        );

        $oauth = array_merge($oauth, $params);
        $baseStr = generateBaseString($method, $url, $oauth);

        $oauth['oauth_signature'] = generateSignature($baseStr, $accessB);
        ksort($oauth);

        $authHeader = 'OAuth ';
        foreach ($oauth as $key => $value) {
            $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        return substr($authHeader, 0, -2);
    }

    function generateBaseString($method, $url, $params)
    {
        $url = parse_url($url);
        if (isset($url['query'])) {
            parse_str($url['query'], $params2);
            $params = array_merge($params, $params2);
        }
        ksort($params);
        $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
        $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
        foreach ($params as $key => $value) {
            $baseStr .= rawurlencode(
                rawurlencode($key) . '=' . rawurlencode($value) . '&'
            );
        }
        return substr($baseStr, 0, -3);
    }

	function generateSignature($baseStr, $accessB)
    {
        
        $signingKey =  $accessB[0]->ConsumerSecret . '&' . $accessB[0]->TokenSecret;
        return base64_encode(
            hash_hmac(
                'sha1',
                $baseStr,
                $signingKey,
                true
            )
        );
    }

?>

<?php get_footer();
