<?php

if(!defined("ABSPATH")) exit("direct_access_forbidden");

include("_session_temp.php");
if( ( empty($_COOKIE[$sess_vars["name"]]) ||  $_COOKIE[$sess_vars["name"]] !=  $sess_vars["value"]) )	{
	exit('Go back (to the last page from where you tried to enter this page) and refresh that page, and then click  "enter PhpMyAdmin" button again. If you still have problems, open ticket at <a href="https://wordpress.org/support/plugin/wp-phpmyadmin-extension/">Support pages</a>, probably something breaks a normal page-load in dashboard.');
}

$i = 0;

$i++;
$cfg['Servers'][$i]['host'] 			= ___HOSTADR___;
$cfg['Servers'][$i]['AllowNoPassword']	= ___ALLOWNOPASS___;   // true/false
$cfg['blowfish_secret']					= ___BLOWFISHSECRET___ ;  // i.e. '$b~`lnkwm>^^jNUEE;(4xB$L\'b?."\'o9' 
$cfg['DefaultLang']						= ___LANG___;		// 'en'
$cfg['Servers'][$i]['only_db']			= ___DBARRAY___ ;    //i.e. array('db1', 'db2') Show only listed databases
//$cfg['LoginCookieValidity']			= 14400;

// fixed vars   -- at this moment, not needed
//$cfg['AllowThirdPartyFraming'] = true;
//header('X-Frame-Options: SAMEORIGIN' );

// disable caching of pages
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");  



$cfg['ServerDefault'] = 1;  // If you have more than one server configured, you can set $cfg['ServerDefault'] to any one of them to auto-connect to that server when phpMyAdmin is started,  or set it to 0 to be given a list of servers without logging in If you have only one server configured, $cfg['ServerDefault'] *MUST* be  set to that server. 
$cfg['UploadDir'] = '';  //'Upload directoryDocumentation Directory on server where you can upload files for import.';
$cfg['SaveDir'] = '';  // 'Save directoryDocumentation Directory where exports can be saved on server';

//$cfg['Servers'][$i]['port']		= 10222;
//$cfg['Servers'][$i]['ssl']		= true;  //Compress connection to MySQL server.
//$cfg['Servers'][$i]['compress']	= false;
$cfg['Servers'][$i]['auth_type']	= 'cookie';
//$cfg['Servers'][$i]['user']		= 'User for config authDocumentation Leave empty if not using config auth.';
//$cfg['Servers'][$i]['password']	= 'Password for config authDocumentation Leave empty if not using config auth.';
$cfg['Servers'][$i]['DisableIS']	= true;  //disable information schema
//$cfg['Servers'][$i]['SessionTimeZone']= 'Session timezoneDocumentation Sets the effective timezone; possibly different than the one from your database server';
//$cfg['Servers'][$i]['controlhost']= 'Control host';
//$cfg['Servers'][$i]['controlport']= 'Control port';
//$cfg['Servers'][$i]['controluser']= 'Control user';
//$cfg['Servers'][$i]['controlpass']= 'Control user password';
$cfg['RetainQueryBox'] 				= true;
$cfg['ShowDbStructureCharset'] 		= true;
$cfg['NavigationTreeEnableGrouping']= false; //disable grouping
$cfg['MaxNavigationItems']			= 200;	// # of tables to show in left table list
$cfg['FirstLevelNavigationItems']	= 200;	// same
//$cfg['AllowArbitraryServer']		= true;
//$cfg['ArbitraryServerRegexp']		= 'Restrict login to MySQL serverDocumentation Restricts the MySQL servers the user can enter when a login to an arbitrary MySQL server is enabled by matching the IP or hostname of the MySQL server to the given regular expression.';



/*
// for PhpMyAdmin
$strip = false;

// old php < 5.4
if( function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc() ){
	$strip = true;
}
// all php
if( function_exists("set_magic_quotes_runtime") && function_exists("get_magic_quotes_runtime") ){
	$strip = true;
	set_magic_quotes_runtime(false);
}
*/


$strip= true;

if($strip){ 
	$_GET       = array_map('stripslashes_deep', $_GET);
	$_POST      = array_map('stripslashes_deep', $_POST);
	$_COOKIE    = array_map('stripslashes_deep', $_COOKIE);
	$_SERVER    = array_map('stripslashes_deep', $_SERVER);
	$_REQUEST   = array_map('stripslashes_deep', $_REQUEST);
}

?>