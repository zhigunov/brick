<?php
    if($_POST['TokenValue']){
        $url = 'https://api.bricklink.com/api/store/v2/orders?status=PAID';
        if(function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders?status=PAID', array('status'=>"PAID"))
            ));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $output = curl_exec($ch);
            echo curl_error($ch);
            curl_close($ch);
        }
        echo json_decode($output)->meta->message;
    }
    if($_POST['login']){
        $username = $_POST['login'];
        $password = $_POST['password'];     
        $host_api = "https://ssapi.shipstation.com/orders";
        $param = 'page=1&pageSize=1';   
        $curl = curl_init($host_api);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        if($orders_arr = json_decode($result)->orders){
            echo 'OK';
        }else{
            echo 'BAD_OAUTH_REQUEST';
        }
    }
	if($_POST['ecd_subscription_key']){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ecomdash.azure-api.net/api/carriers",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/xml",
            "Ocp-Apim-Subscription-Key: " . $_POST['Ocp_Apim_Subscription_Key'],
            "ecd-subscription-key: " . $_POST['ecd_subscription_key']
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if($orders_arr = json_decode($response)->status){
            echo 'OK';
        }else{
            echo 'BAD_OAUTH_REQUEST';
        }
    }
?>

<?php
	function generatePassword($length = 8){
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

    function generateAuth($method, $url, $params)
    {
        $oauth = array(
            'oauth_consumer_key' => $_POST['ConsumerKey'],
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => (string)time(),
            'oauth_nonce' => md5(mt_rand()),
            'oauth_version' => '1.0',
            'oauth_token' => $_POST['TokenValue']
        );

        $oauth = array_merge($oauth, $params);
        $baseStr = generateBaseString($method, $url, $oauth);

        $oauth['oauth_signature'] = generateSignature($baseStr);
        ksort($oauth);

        $authHeader = 'OAuth ';
        foreach ($oauth as $key => $value) {
            $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        return substr($authHeader, 0, -2);
    }

    function generateBaseString($method, $url, $params)
    {
        $url = parse_url($url);
        if (isset($url['query'])) {
            parse_str($url['query'], $params2);
            $params = array_merge($params, $params2);
        }
        ksort($params);
        $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
        $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
        foreach ($params as $key => $value) {
            $baseStr .= rawurlencode(
                rawurlencode($key) . '=' . rawurlencode($value) . '&'
            );
        }
        return substr($baseStr, 0, -3);
    }

    function generateSignature($baseStr)
    {
        
        $signingKey =  $_POST['ConsumerSecret'] . '&' . $_POST['TokenSecret'];
        return base64_encode(
            hash_hmac(
                'sha1',
                $baseStr,
                $signingKey,
                true
            )
        );
    }

?>