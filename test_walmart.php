<?php
	$URL = 'https://marketplace.walmartapis.com/v3/orders?createdStartDate=2018-01-01T10:30:15Z&status=Acknowledged';
	$RequestMethod = 'GET';
	$Timestamp = round(microtime(true) * 1000); //Current system timestamp

	echo '<pre>';

	var_dump(array(
	    "Cache-Control: no-cache",
	    "WM_CONSUMER.CHANNEL.TYPE: 9ebb75c2-1baf-4238-89f7-59a6c37bc347",
	    "WM_CONSUMER.ID: 27ea86fc-c6d9-4c1e-823e-d85b6d46fca9",
	    "WM_QOS.CORRELATION_ID: 123456abcdef",
	    "WM_SEC.AUTH_SIGNATURE: " . _GetWalmartAuthSignature($URL, $RequestMethod, $Timestamp),
	    "WM_SEC.TIMESTAMP: " . $Timestamp,
	    "WM_SVC.NAME: Walmart Marketplace")
	);

	echo '<br><br>';

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => $URL,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "Cache-Control: no-cache",
	    "WM_CONSUMER.CHANNEL.TYPE: 9ebb75c2-1baf-4238-89f7-59a6c37bc347",
	    "WM_CONSUMER.ID: 27ea86fc-c6d9-4c1e-823e-d85b6d46fca9",
	    "WM_QOS.CORRELATION_ID: 123456abcdef",
	    "WM_SEC.AUTH_SIGNATURE: " . (string)_GetWalmartAuthSignature($URL, $RequestMethod, $Timestamp),
	    "WM_SEC.TIMESTAMP: " . $Timestamp,
	    "WM_SVC.NAME: Walmart Marketplace"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}

	function _GetWalmartAuthSignature($URL, $RequestMethod, $Timestamp) {
		$WalmartPrivateKey = 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJ/tW1d5GulzOlqLsJA02egiEx1NiXkEQspAG2SZbME5HW2Qk0jdEdaN2V1Y+48V/bFnvqMk+Scox2Igbkza+YrUABCp2m/ZsccSeLcYy12vgN6M6Y0lXF+CToCnB7vDNDYxNzpsUCjnFXSwIcYdGV7XtHbY2jGkGNHK1vRXsuKHAgMBAAECgYBqPJNT3TgaYhgec/L7KJCkWpZV/d7XjBQOJF3OCvkqthUtZj/gxDIIU7xT/TtGhrY69g62c2YrNDSWo7KBzWi8V/0kqB7ZgKIkhyHVbR1K8v1sok6Uq8IQ1b6URPLesLVhg6mFrQYbjuFVoR73+prOlcQyxX/xm7vdKqbdKl2WEQJBAOcG0rem6jdmRwyRgAWI3nPykwo4ixhL42EeTzXLjH5gGIKeYHJCaIGOfO5jsQHxKsOmIFwgGwmx2SR9TxzP9M8CQQCxNwFQAFF/A9pudAiuBROdeQuxYsa6coYpn8QwSr2WRUsT8DsrYpJjlNqohK2Y4rPmyN7Rh10swUClPpdT3JTJAkEA40slOaawvLyVnlnwWIGmSvEXlXC3+LmBBeXqDIUsvpEQzEHBpeiMJRro8ymF1uFNL5xcLgcwUsN1XZ8jnLdEpQJAHZA0itkSv2KwLN70l7tHE20fEz+MMYxrb0Q04W2GhSwYI0JfTJSMIUqy04wWvbuMEYMPENtJIu82TprLefUBaQJAEyDSFvbP81IRZDR73hNxNXtWMSxvuPGts/GAXLlxqTF6wVor9CxOB6P+067Y2NTuYmbqVuenCmJ2D6ngfJ0hsg==';
		$WalmartConsumerID = '27ea86fc-c6d9-4c1e-823e-d85b6d46fca9';
		// CONSTRUCT THE AUTH DATA WE WANT TO SIGN
		$AuthData = $WalmartConsumerID."\n";
		$AuthData .= $URL."\n";
		$AuthData .= $RequestMethod."\n";
		$AuthData .= $Timestamp."\n";
		// GET AN OPENSSL USABLE PRIVATE KEY FROMM THE WARMART SUPPLIED SECRET
		$Pem = _ConvertPkcs8ToPem(base64_decode($WalmartPrivateKey));
		$PrivateKey = openssl_pkey_get_private($Pem);
		// SIGN THE DATA. USE sha256 HASH
		$Hash = defined("OPENSSL_ALGO_SHA256") ? OPENSSL_ALGO_SHA256 : "sha256";
		if (!openssl_sign($AuthData, $Signature, $PrivateKey, $Hash))
		{ // IF ERROR RETURN NULL return null; 
		}
		//ENCODE THE SIGNATURE AND RETURN
		return base64_encode($Signature);
	}
	function _ConvertPkcs8ToPem($der)
		{
		static $BEGIN_MARKER = "-----BEGIN PRIVATE KEY-----";
		static $END_MARKER = "-----END PRIVATE KEY-----";
		$key = base64_encode($der);
		$pem = $BEGIN_MARKER . "\n";
		$pem .= chunk_split($key, 64, "\n");
		$pem .= $END_MARKER . "\n";
		return $pem;
		}
	

	echo '<br>END';
?>