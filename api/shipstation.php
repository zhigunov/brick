<table border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
	<tr>
		<td>Order #</td>
		<td>Total Items</td>
		<td>Unique Items (Lots)</td>
		<td>Shipping Method</td>
		<td>customer name</td>
		<td>customer address</td>
		<td>customer e-mail</td>
		<td>value</td>
		<td>shipping total</td>
		<td>order date</td>
	</tr>
<?php
	$username = "f02364107d96482abef1930df5b043c0";
	$password = "e32bd85d5e9f4709bf99557430f5cf52";     
	$host_api = "https://ssapi.shipstation.com/orders?store_id=26624";
	$param = '';   
	 
	// авторизация
	$curl = curl_init($host_api);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);       
	// get запрос
	curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	// вывести результат
	// echo "<pre>";
	// var_dump(json_decode($result));
	$orders_arr = json_decode($result)->orders;

	foreach ($orders_arr as $orders) {
		$total_items = 0;
		foreach ($orders->items as $item) {
			$total_items += $item->quantity;
		}
		?>
		<tr>
			<td><?php echo $orders->orderId; ?></td>
			<td><?php echo $total_items; ?></td>
			<td><?php echo count($orders->items); ?></td>
			<td><?php echo $orders->requestedShippingService; ?></td>
			<td><?php echo $orders->customerUsername; ?></td>
			<td><?php echo $orders->shipTo->name . ' ' . $orders->shipTo->country . ' ' . $orders->shipTo->city  . ' ' . $orders->shipTo->street1; ?></td>
			<td><?php echo $orders->customerEmail; ?></td>
			<td><?php echo $orders->orderTotal; ?></td>
			<td><?php echo $orders->shippingAmount; ?></td>
			<td><?php echo $orders->createDate; ?></td>
		</tr>
		<?php
	}

?>
</table>