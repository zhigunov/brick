<?php
require_once "OAuth.php";

// ������ ���������.
define("ENCODING", "windows-1251"); // ��������� �����. ���� � ��� UTF-8, �� �� �������!
define("TAG", "support"); // ���, �� �������� ������������ ���������� ������.

// ��������� OAuth. ��������� �� �������� (�������� SECRET).
define("OA_CONSUMER_KEY", "JId0zVAbQCVnqjD9OlvM"); // ��������� OAuth-�������.
define("OA_CONSUMER_SECRET", "qocMBQg1P17CBcdVsJizsNPnlGbTU4fvlGxAszmzB5");
define("OA_URL_REQ_TOK", "http://api.rutvit.ru/oauth/request_token");
define("OA_URL_AUTH_TOK", "https://api.rutvit.ru/oauth/authorize");
define("OA_URL_ACCESS_TOK", "http://api.rutvit.ru/oauth/access_token");

// ��� ������ � OAuth ��� ��������� 3 ����������, ����������� ���� ��������
// ����� ���������� ������� (��� �������� - ������ �� � ������).
session_start();
$S_MSG         = &$_SESSION['msg'];
$S_REQUEST_TOK = &$_SESSION['REQUEST_TOK'];
$S_ACCESS_TOK  = &$_SESSION['ACCESS_TOK'];

// ����: 
//   form_is_sent -> 
//     fetch_request_token -> 
//       authorize_request_token (����� �������) ->
//         fetch_access_token (����� request_token �� access_token) ->
//           send_msg (����� API)
// ���:
//   form_is_sent ->
//     send_msg (����� API)
$action = @$_GET['action'];
while ($action) {
	switch ($action) {

		// 1. ��������� �������� �����. ����������, � ������ ���� ��������:
		//    ���� � OAuth, ���� � �������� ��������� ����� API.
		case 'form_is_sent': {
			// ��������� ��������� � ������, ��� ��� ����������� �����.
			$S_MSG = $_POST['msg'];
			if ($S_ACCESS_TOK && $S_ACCESS_TOK->secret) {
				// ������������ ��� ��������� ����������� � ������� ������.
				$action = 'send_msg';
			} else {
				// ����������� ��� �� ���������, ��������� ��������� OAuth.
				$action = 'fetch_request_token';
			}
			break;
		}

		// 2. ��������� ��������� Request Token.
		//    ���������� � Service Provider ����� ����� � �������� �����.
		case 'fetch_request_token': {
			// ��������� ������ �� ��������� Request Token.
			$consumer = new OAuthConsumer(OA_CONSUMER_KEY, OA_CONSUMER_SECRET);
			$req = OAuthRequest::from_consumer_and_token(
				$consumer, NULL, 
				"GET", OA_URL_REQ_TOK
			);
			// ��������� � ������ �������� �������, ����� �� ���������.
			$req->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, NULL);
			// �������� Request Token � ���������� ��� �� �����������.
			$parsed = OAuthUtil::parse_parameters(file_get_contents($req->to_url()));
			$S_REQUEST_TOK = new OAuthToken($parsed['oauth_token'], $parsed['oauth_token_secret']);
			// ��������� � ���������� ���������.
			$action = 'authorize_request_token';
			break;
		}

		// 3. ����������� (������������� �������������) Request Token's ����� ��������.
		//    ������������ ������� �� Service Provider ��� �������������� ������� �������������.
		//    ��� �������� ������� � GET-���������� ����� action=fetch_access_token.
		case 'authorize_request_token': {
			// �� ���� URL �������� ������� ����� �������������.
			$callbackUrl = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['SCRIPT_NAME']}"
				. "?action=fetch_access_token";
			// �������� callback-URL � ���������� (�������� OAuth 1.0; � 1.0a - ��� �� ���!).
			$authUrl = OA_URL_AUTH_TOK . "?"
				. "&oauth_token={$S_REQUEST_TOK->key}"
				. "&oauth_callback=" . urlencode($callbackUrl);
			// ���������� ��������.
			header("Location: $authUrl");
			exit();
		}
		
		// 4. ����� Request Token �� Access Token � ������ Access Token � ������.
		//    ���� ��������� �� ��������� ����� ������������� ������� �������������.
		case 'fetch_access_token': {
			$consumer = new OAuthConsumer(OA_CONSUMER_KEY, OA_CONSUMER_SECRET);
			$req = OAuthRequest::from_consumer_and_token(
				$consumer, $S_REQUEST_TOK, 
				"GET", OA_URL_ACCESS_TOK,
				array() // ���. ���������
			);
			$req->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, $S_REQUEST_TOK);
			// ��������� ������ � ���������� Access Token � ������.
			$parsed = OAuthUtil::parse_parameters(file_get_contents($req->to_url()));
			$S_ACCESS_TOK = new OAuthToken($parsed['oauth_token'], $parsed['oauth_token_secret']);
			// ������� � �������� ���������.
			$action = 'send_msg';
			break;
		}

		// 5. ���������� ���������.
		//    ����������� URL API � OAuth-���������.
		case 'send_msg': {
			$consumer = new OAuthConsumer(OA_CONSUMER_KEY, OA_CONSUMER_SECRET);
			$req = OAuthRequest::from_consumer_and_token(
				$consumer, $S_ACCESS_TOK, 
				'POST', 'http://api.rutvit.ru/statuses/update.xml', 
				array('status' => "#" . TAG . " " . iconv(ENCODING, "UTF-8", $S_MSG))
			);
			$req->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, $S_ACCESS_TOK);
			// ���������� POST-������.
			$h = curl_init();
			curl_setopt($h, CURLOPT_URL, $req->get_normalized_http_url());
			curl_setopt($h, CURLOPT_POST, true);
			curl_setopt($h, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($h, CURLOPT_POSTFIELDS, $req->to_postdata());
			$resp = curl_exec($h);
			$code = curl_getinfo($h, CURLINFO_HTTP_CODE);
			// ��� ������ - �������� ������� �� �������� � ��������.
			if ($code != 200) {
				e($resp);
				exit();
			}
			header("Location: {$_SERVER['SCRIPT_NAME']}");
			exit();
		}

		// ��� ������� �� ������ ���������.
		default: {
			die("Invalid action!");
		}
	}
}


// �������� ��� ��������� �����.
$text = file_get_contents("http://api.rutvit.ru/search.xml?rpp=5&q=" . urlencode("#" . TAG));
$TWEETS = new SimpleXMLElement($text);

// Shortcut ��� ������ ��������� � �������������� � ���������.
function e($text, $quote = 1)
{
	$text = iconv("utf-8", ENCODING, $text);
	echo $quote? htmlspecialchars($text) : $text;
}
?>


<style>
.hiddenLink { display: none }
</style>

<div style="border: 1px solid black; padding: 0.5em">
<?foreach ($TWEETS->status as $tweet) {?>
	<div style="margin-bottom: 6px">
		<b><?e($tweet->user->screen_name)?>:</b> 
		<?e($tweet->text_formatted, 0)?>
	</div>
<?}?>
<form method="post" action="<?e($_SERVER['SCRIPT_NAME'])?>?action=form_is_sent" style="margin: 1em 0 0 0">
	<input type="text" size="30" name="msg" />
	<input type="submit" value="���������" />
</form>
</div>
