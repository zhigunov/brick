<style type="text/css">
	
	#wr-tabs{
	width: 100%;
	margin: 40px auto 0;
}
	#wr-tabs .tabs{
		background-color: #fff;
		margin-bottom: 3px;
	}
	#wr-tabs .tabs:after{
		content: "";
		display: block;
		clear: both;
		height: 0;
	}
		#wr-tabs .tabs .tab{
			float: left;
			cursor: pointer;
			border-right: 1px solid #f2f4f9;
			padding: 10px 20px;
		}
		#wr-tabs .tabs .tab:last-child{
			border-right: none;
		}
		#wr-tabs .tabs .tab:hover,
		#wr-tabs .tabs .tab.active{
			background-color: #c7e7f9;
			color: #1d7ee4;
		}
	#wr-tabs .content{
		background-color: #fff;
	}
		#wr-tabs .content .tab-cont{
			display: none;
			padding: 15px 10px;
		}
		#wr-tabs .content .tab-cont.active{
			display: block;
		}

</style>
<script type="text/javascript">
	$(function(){

	$("#wr-tabs").on("click", ".tab", function(){

		var tabs = $("#wr-tabs .tab"),
		    cont = $("#wr-tabs .tab-cont");

		// Удаляем классы active
		tabs.removeClass("active");
		cont.removeClass("active");
		// Добавляем классы active
		$(this).addClass("active");
		cont.eq($(this).index()).addClass("active");

		return false;
	});
});
</script>
<div id="wr-tabs">
	<div class="tabs">
		<div class="tab active">Вкладка №1</div>
		<div class="tab">Вкладка №2</div>
	</div>
	<div class="content">
		<div class="tab-cont active">
			



<?php
	$url = 'https://api.bricklink.com/api/store/v2/orders';
?> 
	<table border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
		<tr>
			<td>Order #</td>
			<td>Total Items</td>
			<td>Unique Items (Lots)</td>
			<td>Shipping Method</td>
			<td>customer name</td>
			<td>customer address</td>
			<td>customer e-mail</td>
			<td>value</td>
			<td>shipping total</td>
			<td>order date</td>
		</tr>
	<?php

	if(function_exists('curl_init')) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders', array())
		));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch);
		echo curl_error($ch);
		curl_close($ch);
	}
	$orders_arr = array_slice(array_reverse(json_decode($output)->data),0, 10);
	// echo '<pre>';
	// var_dump($orders_arr[703]);

	foreach ($orders_arr as $orders) {
		$url = 'https://api.bricklink.com/api/store/v2/orders/' . $orders->order_id;
		if(function_exists('curl_init')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/' . $orders->order_id, array())
			));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch);
			echo curl_error($ch);
			curl_close($ch);
			$order_obj = json_decode($output)->data;
		}
		$url = 'https://api.bricklink.com/api/store/v2/orders/8843859/items';
		if(function_exists('curl_init')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/orders/8843859/items', array())
			));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch);
			echo curl_error($ch);
			curl_close($ch);
			$items_arr = json_decode($output)->data;
		}
		?>
		<tr>
			<td><?php echo $orders->order_id; ?></td>
			<td><?php echo $orders->total_count; ?></td>
			<td><?php echo $orders->unique_count; ?></td>
			<td><?php echo $order_obj->shipping->method; ?></td>
			<td><?php echo $order_obj->buyer_name; ?></td>
			<td><?php echo $order_obj->shipping->address->name->full . ' ' . $order_obj->shipping->address->full; ?></td>
			<td><?php echo $order_obj->buyer_email; ?></td>
			<td><?php echo $order_obj->cost->grand_total; ?></td>
			<td><?php echo $order_obj->cost->shipping; ?></td>
			<td><?php echo $order_obj->date_ordered; ?></td>
		</tr>
		<?php
	}
	?>
	</table>
		</div>
		<div class="tab-cont">
			<table border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
	<tr>
		<td>Order #</td>
		<td>Total Items</td>
		<td>Unique Items (Lots)</td>
		<td>Shipping Method</td>
		<td>customer name</td>
		<td>customer address</td>
		<td>customer e-mail</td>
		<td>value</td>
		<td>shipping total</td>
		<td>order date</td>
	</tr>
<?php
	$username = "f02364107d96482abef1930df5b043c0";
	$password = "e32bd85d5e9f4709bf99557430f5cf52";     
	$host_api = "https://ssapi.shipstation.com/orders?store_id=26624";
	$param = '';   
	 
	// авторизация
	$curl = curl_init($host_api);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);       
	// get запрос
	curl_setopt($curl, CURLOPT_URL, "$host_api?$param");       
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	// вывести результат
	// echo "<pre>";
	// var_dump(json_decode($result));
	$orders_arr = json_decode($result)->orders;

	foreach ($orders_arr as $orders) {
		$total_items = 0;
		foreach ($orders->items as $item) {
			$total_items += $item->quantity;
		}
		?>
		<tr>
			<td><?php echo $orders->orderId; ?></td>
			<td><?php echo $total_items; ?></td>
			<td><?php echo count($orders->items); ?></td>
			<td><?php echo $orders->requestedShippingService; ?></td>
			<td><?php echo $orders->customerUsername; ?></td>
			<td><?php echo $orders->shipTo->name . ' ' . $orders->shipTo->country . ' ' . $orders->shipTo->city  . ' ' . $orders->shipTo->street1; ?></td>
			<td><?php echo $orders->customerEmail; ?></td>
			<td><?php echo $orders->orderTotal; ?></td>
			<td><?php echo $orders->shippingAmount; ?></td>
			<td><?php echo $orders->createDate; ?></td>
		</tr>
		<?php
	}

?>
</table>
		</div>
	</div>
</div>




	<?php
	function generatePassword($length = 8){
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}

	function generateAuth($method, $url, $params)
    {
    	$oauth = array(
            'oauth_consumer_key' => '0F59AFA18DE442C7B3F743ADE0C2ED9F',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => (string)time(),
            'oauth_nonce' => md5(mt_rand()),
            'oauth_version' => '1.0',
            'oauth_token' => 'C94364E4991C4AA383180D0DE316C07E'
        );

        $oauth = array_merge($oauth, $params);
        $baseStr = generateBaseString($method, $url, $oauth);

        $oauth['oauth_signature'] = generateSignature($baseStr);
        ksort($oauth);

        $authHeader = 'OAuth ';
        foreach ($oauth as $key => $value) {
            $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        return substr($authHeader, 0, -2);
    }

    function generateBaseString($method, $url, $params)
    {
        $url = parse_url($url);
        if (isset($url['query'])) {
            parse_str($url['query'], $params2);
            $params = array_merge($params, $params2);
        }
        ksort($params);
        $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
        $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
        foreach ($params as $key => $value) {
            $baseStr .= rawurlencode(
                rawurlencode($key) . '=' . rawurlencode($value) . '&'
            );
        }
        return substr($baseStr, 0, -3);
    }

	function generateSignature($baseStr)
    {
        
        $signingKey =  '90305EBE27A3413AACBB17BB83ED0408' . '&' . 'AF30A745D05348DDAF590541556AC960';
        return base64_encode(
            hash_hmac(
                'sha1',
                $baseStr,
                $signingKey,
                true
            )
        );
    }

?>