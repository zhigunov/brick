<?php 
require_once( '/home/mplaceconnect/marketplaceconnect.lefttwin.org/wp-load.php' );
global $wpdb;

$users = get_users();

$wpdb->query("TRUNCATE `wp_brick_inventory`");

foreach ($users as $user) {
    $accessB = $wpdb->get_results("SELECT * FROM wp_brick_access WHERE user_id = ".$user->data->ID);
    $accessE = $wpdb->get_results("SELECT * FROM wp_ecom_access WHERE user_id = ".$user->data->ID);

    if(!isset($accessE[0]->ecd_subscription_key)){
        continue;
    }

    /////////////////////запись инвентория в базу

	$url = 'https://api.bricklink.com/api/store/v2/inventories';
    if(function_exists('curl_init')) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: '. generateAuth('GET', 'https://api.bricklink.com/api/store/v2/inventories', array(), $accessB)
        ));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        echo curl_error($ch);
        curl_close($ch);
    }
    $items_arr = json_decode($output)->data;

    foreach ($items_arr as $item) {
    	$count = 0;
    	foreach ($items_arr as $allIteml) {
    		if($item->remarks === $allIteml->remarks){
    			$count += 1;
    		}
    	}

    	if($count > 1){
    		continue;
    	}
    	$count = 0;


    	if($item->remarks != ''){
    		$selectItem = $wpdb->get_results("SELECT * FROM wp_brick_inventory WHERE sku = '".$item->remarks."'");
	    	if($selectItem){
				$wpdb->query("UPDATE `wp_brick_inventory` SET `quantity`='".$item->quantity."' WHERE sku = '".$item->remarks."'");
	    	}else{
				$wpdb->query("INSERT INTO `wp_brick_inventory` (`inventory_id`, `sku`, `quantity`) VALUES ('".$item->inventory_id."', '".$item->remarks."','".$item->quantity."')");
	    	}
    	}
    }
}

function generatePassword($length = 8){
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $string;
}

function generateAuth($method, $url, $params, $accessB)
{
    $oauth = array(
        'oauth_consumer_key' => $accessB[0]->ConsumerKey,
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_timestamp' => (string)time(),
        'oauth_nonce' => md5(mt_rand()),
        'oauth_version' => '1.0',
        'oauth_token' => $accessB[0]->TokenValue
    );

    $oauth = array_merge($oauth, $params);
    $baseStr = generateBaseString($method, $url, $oauth);

    $oauth['oauth_signature'] = generateSignature($baseStr, $accessB);
    ksort($oauth);

    $authHeader = 'OAuth ';
    foreach ($oauth as $key => $value) {
        $authHeader .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
    }
    return substr($authHeader, 0, -2);
}

function generateBaseString($method, $url, $params)
{
    $url = parse_url($url);
    if (isset($url['query'])) {
        parse_str($url['query'], $params2);
        $params = array_merge($params, $params2);
    }
    ksort($params);
    $baseUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];
    $baseStr = strtoupper($method) . '&' . rawurlencode($baseUrl) . '&';
    foreach ($params as $key => $value) {
        $baseStr .= rawurlencode(
            rawurlencode($key) . '=' . rawurlencode($value) . '&'
        );
    }
    return substr($baseStr, 0, -3);
}

function generateSignature($baseStr, $accessB)
{
    
    $signingKey =  $accessB[0]->ConsumerSecret . '&' . $accessB[0]->TokenSecret;
    return base64_encode(
        hash_hmac(
            'sha1',
            $baseStr,
            $signingKey,
            true
        )
    );
}

?>